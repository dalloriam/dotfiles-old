# cleans JSON file for display
clean_json() {
    cat $1 | jq . >> temp.json
    mv temp.json $1
}
alias clean=clean_json


# Converts WebM files to MP4
webmTomp4 () {
    ffmpeg -i "$1".webm -qscale 0 "$1".mp4
}
alias convwebm=webmTomp4
