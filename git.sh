# GIT ALIASES
alias gst='git status'
alias ga='git add'
alias gc='notodo && git commit -m '

# GIT AUTOCOMPLETE
if [ -f ~/.git-completion.bash  ]; then
  . ~/.git-completion.bash
fi
