# BASIC ZSH CUSTOMIZATION
###################################

# Includes "homebrewed scripts" directory in path
export PATH=$PATH:$HOME/bin:$HOME/dev/bin

export CLICOLOR=1

# NAVIGATION ALIASES
alias ls='ls -GF'
alias sl='ls -GF'
alias l='ls -GF'
alias s='ls -GF'
alias clera='clear'
alias celar='clear'
alias vim='nvim'


# HISTORY SETTINGS

# don't put duplicate lines in the history. See bash(1) for more options
HISTCONTROL=ignoredups
# ... and ignore same sucessive entries.
HISTCONTROL=ignoreboth

# Expand the history size
HISTFILESIZE=10000
HISTSIZE=100
