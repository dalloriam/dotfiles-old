#!/bin/bash
# Inspired by https://github.com/jessfraz/dotfiles

del_stopped(){
    local name=$1
    local state
    state=$(docker inspect --format "{{.State.Running}}" "$name" 2>/dev/null)

    if [[ "$state" == "false" ]]; then
        docker rm "$name"
    fi
}

elasticsearch() {
    del_stopped elasticsearch

    docker run --rm -it \
        -p 9200:9200 \
        -p 9300:9300 \
        -e "discovery.type=single-node" \
        elasticsearch:6.2.2
}

ubuntu() {
    docker run --rm -it \
    ubuntu
}
